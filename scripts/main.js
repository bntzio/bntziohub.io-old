/**
 * Modules
 */

var React = require('react');
var ReactDOM = require('react-dom');
var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var Navigation = ReactRouter.Navigation;
var createBrowserHistory = require('history/lib/createBrowserHistory');

/**
 * LandingPage
 * <LandingPage/>
 */

var LandingPage = React.createClass({
	firePopup : function() {
		console.log("clicked!")
	},
	render: function() {
		return (
			<div>
				<div className="top-container">
					<section id="sidePanel" className="sidePanel">
						<img src="./build/img/enrique.jpg" className="sidePanel-avatar"/>
		      	<p className="sidePanel-title"><b>Nice to meet you</b></p>
		      	<p className="sidePanel-description">
			      	I'm glad you found me! My name is Enrique Benitez, 
			      	also known as <a href="https://twitter.com/bntzio" target="_blank" id="twtr">@bntzio</a> in 
			      	the tech world, I am a full-stack web/app developer, designer, growth hacker and entrepreneur.
		      	</p>
		      	<ul className="sidePanel-list">
		        	<a href="http://bntz.io/blog"><li className="sidePanel-list-item">BLOG</li></a>
              <a href="#"><li className="sidePanel-list-item">ABOUT</li></a>
		        	<li className="sidePanel-list-item">COURSES</li>
		        	<li className="sidePanel-list-item">STARTUPS</li>
		        	<li className="sidePanel-list-item">CHALLENGES</li>
		      	</ul>
	    		</section>
	    		<section className="hero">
	    			<div className="hero-container">
	    				<h1 className="hero-container-title"><a href="http://bntz.io">BNTZIO</a></h1>
	    				<ul className="hero-container-list">
	    					<a href="http://bntz.io/blog"><li className="hero-container-list-item">Blog</li></a>
	    					<a href="#"><li className="hero-container-list-item">About</li></a>
	          				<li className="hero-container-list-item">Courses</li>
	          				<li className="hero-container-list-item">Startups</li>
	          				<li className="hero-container-list-item">Challenges</li>
	    				</ul>
	    				<i id="mainPanel" className="fa fa-bars hero-menu"></i>
	    			</div>
	    			<p className="hero-description">I MAKE SHIT HAPPEN.</p>
	      		<i className="fa fa-angle-down hero-arrow"></i>
	    		</section>
				</div>
	  		<section className="hacker">
	    		<img src="./build/img/rocket.gif" className="hacker-image"/>
	    		<h2 className="hacker-title">GROWTH HACKER</h2>
	    		<p className="hacker-description">
	    			My philosophy when launching a product is to apply growth hacking techniques that 
	    			will impact the growth in record time, it's not just building and launching, it's 
	    			building, launching and growing!
	    		</p>
	  		</section>

			  <section className="entrepreneur">
			    <img src="./build/img/experiment.gif" className="entrepreneur-image"/>
			    <h2 className="entrepreneur-title">ENTREPRENEUR</h2>
			    <p className="entrepreneur-description">
			    	I focus on business development, applying 'Lean' frameworks and methodologies 
			    	that will increment the success rate of the product, minifying the risk of failure 
			    	at it's most, that's a must... Well, sometimes I just <a href="https://fuckinglaunch.com" target="_blank">FUCKING LAUNCH</a> to be honest.
			    </p>
			  </section>

			  <section className="programmer">
			  <img src="./build/img/code.gif" className="programmer-image"/>
			   <h2 className="programmer-title">PROGRAMMER</h2>
			   <p className="programmer-description">
			     Coding is the closest thing we have to a superpower, I'm a hacker, 
			     I build things, from front-end to back-end... AngularJS, React, jQuery, 
			     Ruby on Rails, NodeJS, Unity3D, Swift... I can go on.
			   </p>
			  </section>

			  <section className="designer">
			    <img src="./build/img/bezier.gif" className="designer-image"/>
			    <h2 className="designer-title">DESIGNER</h2>
			    <p className="designer-description">
			    	Design is everywhere, it's thinking made visual, I love good design, 
			    	minimalist and clean user interfaces, user experience over everything, 
			    	typography, interaction and crisp pixel-perfect icons. You know, design became 
			    	the new language of business.
			    </p>
			  </section>

			  <footer className="footer">
			    <div className="footer-newsletter">
			      <i className="fa fa-bolt footer-bolt"></i>
			      <h2 className="footer-newsletter-title">JOIN THE MAKERS CLUB!</h2>
			      <p className="footer-newsletter-description">Want to receive awesome stuff for <span id="js-rotating">entrepreneurs, coders, designers, growth hackers</span>? ⚒</p>
			      <p className="footer-newsletter-description small">Sign up now and get my <b>free</b> <em id="coding-bricks">Coding Bricks</em> course when its out!</p>
			  
<a className="typeform-share button" href="https://bntzio.typeform.com/to/ccZ3gT" data-mode="1" target="_blank"><i className="fa fa-bolt"></i> Join the club</a>

			      <p className="footer-newsletter-description smaller">I hate spam as well, don't worry! 😊</p>
			    </div>

			    <div className="footer-contact">
			      <i className="fa fa-comments footer-chat"></i>
			      <h5 className="footer-contact-title">WANNA TALK?</h5>
			      <p className="footer-contact-description">I love to talk! Shoot me a message! 😄</p> 
<a className="typeform-share button" href="https://bntzio.typeform.com/to/sIclyd" data-mode="1" target="_blank"><i className="fa fa-paper-plane"></i> Let's do it!</a>            
			    </div>

			    <div className="footer-links">
			      <h5 className="footer-links-title">FIND ME ELSEWHERE</h5>
			      <ul className="footer-links-list">
			        <a href="https://twitter.com/bntzio" target="_blank"><li className="footer-links-list-item twitter"><i className="fa fa-twitter"></i>Twitter</li></a>
			        <a href="https://facebook.com/bntzio.developer" target="_blank"><li className="footer-links-list-item"><i className="fa fa-facebook"></i>Facebook</li></a>
			        <a href="#" target="_blank"><li className="footer-links-list-item"><i className="fa fa-youtube"></i>YouTube</li></a>
			        <a href="https://instagram.com/bntzio" target="_blank"><li className="footer-links-list-item"><i className="fa fa-instagram"></i>Instagram</li></a>
			        <a href="https://github.com/bntzio" target="_blank"><li className="footer-links-list-item"><i className="fa fa-github"></i>Github</li></a>
			        <a href="https://codepen.io/bntzio" target="_blank"><li className="footer-links-list-item"><i className="fa fa-codepen"></i>Codepen</li></a>
			        <a href="https://dribbble.com/bntzio" target="_blank"><li className="footer-links-list-item"><i className="fa fa-dribbble"></i>Dribbble</li></a>
			        <a href="https://www.linkedin.com/in/bntzio" target="_blank"><li className="footer-links-list-item"><i className="fa fa-linkedin"></i>LinkedIn</li></a>
			        <a href="https://medium.com/@bntzio" target="_blank"><li className="footer-links-list-item"><i className="fa fa-medium"></i>Medium</li></a>
			      </ul>
			    </div>
			    <p className="footer-by"><i className="fa fa-code"></i> with <i className="fa fa-heart"></i> and <i className="fa fa-magic"></i></p>
			  </footer>
  		</div>
		)
	}
});

/**
 * NotFound
 * <NotFound/>
 */

var NotFound = React.createClass({
	render: function() {
		return (
			<p>Not found!</p>
		)
	}
});

/**
 * Routes
 */

var routes = (
	<Router history={createBrowserHistory()}>
		<Route path="/" component={LandingPage}/>	
		<Route path="*" component={NotFound}/>
	</Router>
)

ReactDOM.render(routes, document.querySelector('#main'));
