$(function() {
  // variables
  var $heroTitle = $('.hero-container-title');
  var $heroList = $('.hero-container-list');
  var $heroDescription = $('.hero-description');
  var $menuBtn = $('.hero-menu');
  var mainPanel = document.getElementById('mainPanel');
  var sidePanel = document.getElementById('sidePanel');
  var hammerPanel = new Hammer(mainPanel);
  var hammerSide = new Hammer(sidePanel);
  var jelloAnimation = 'animated jello';
  var bounceInDownAnimation = 'animated bounceInDown';
  var fadeInAnimation = 'animated fadeIn';
  var tadaAnimation = 'animated tada'
  var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

  // animate.css
  $heroTitle.addClass(fadeInAnimation).one(animationEnd, 
    function() {
      $(this).removeClass(fadeInAnimation).delay( 200 ).addClass(jelloAnimation).one(animationEnd,
        function() {
          $(this).removeClass(jelloAnimation);
        });
    });

  $heroList.addClass(fadeInAnimation).one(animationEnd, 
    function() {
      $(this).removeClass(fadeInAnimation).one(animationEnd,
        function() {
          $(this).removeClass(fadeInAnimation);
        });
    });
  
  $heroDescription.addClass(bounceInDownAnimation).one(animationEnd,
    function() {
      $(this).removeClass(bounceInDownAnimation);
    });

  $menuBtn.addClass(fadeInAnimation).one(animationEnd,
    function() {
      $(this).removeClass(fadeInAnimation).delay( 1000 ).addClass(tadaAnimation).one(animationEnd,
        function() {
          $(this).removeClass(tadaAnimation);
        });
    });

  // hammer.js
  hammerPanel.on('tap', function() {
    sidePanel.classList.toggle('open');
  });

  // hero menu animation
  $menuBtn.click(function() {
    if ($( this ).hasClass('fa fa-bars hero-menu')) {
      $(this).removeClass('fa fa-bars hero-menu').addClass('fa fa-times hero-menu');
    } else {
      $(this).removeClass('fa fa-times hero-menu').addClass('fa fa-bars hero-menu');
    };
  });

  // morphext
  $("#js-rotating").Morphext({
      animation: "bounceIn",
      separator: ",",
      speed: 2000,
      complete: function () {
      }
    });

});