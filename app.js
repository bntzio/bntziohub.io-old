/**
 * Module dependencies
 */
var express = require('express')

/**
 * Create Express server
 */
var app = express()

/**
 * Express configuration
 */
app.set('port', process.env.PORT || 3000)
app.use(express.static('build'))

/**
 * Initialize Express server
 */
app.listen(app.get('port'), function() {
	console.log('Express server listening on port %d in %s mode', app.get('port'), app.get('env'))
})
